

var a = [4, 3, 2]
var b = a.filter(element => element > 2)

var a = [1, 2, 3]
var b = a.some(element => element == 2)

var a = [2, 2, 3]
var b = a.every(element => element >= 2)


var p = Promise.resolve(1)
var b = await p


var ps = Promise.all([
    Promise.resolve([1,2]),
    Promise.resolve([4,4])
])
var b = await ps





