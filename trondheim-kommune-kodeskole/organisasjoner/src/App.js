import React, { Component } from "react";
import TextField from "@material-ui/core/TextField";
import "./App.css";
import OrganisationListItem from "./OrganisationListItem";
import Button from "@material-ui/core/Button";
import flatten from "flatten";
import { createMuiTheme } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/styles';

const theme = createMuiTheme({
  typography: {
    // Bootstrap har 10px som standardstørrelse, mens Material-UI sin standardstørrelse er 16px
    htmlFontSize: 10,
  },
});

class App extends Component {
  state = {
    organisations: [],
    max: 20,
    activity_types: []
  };
  abortController = null;

  constructor(props) {
    super(props);
    fetch(
      `https://organisasjoner.trondheim.kommune.no/api/organisations/v1/flod_activity_types/`
    )
      .then(r => r.json())
      .then(activity_types =>
        this.setState({ activity_types: activity_types })
      );
  }

  handleInput = async (event) => {
    this.setState({ max: 20 });
    this.setState({ organisations: [] });

    if (this.abortController) {
      this.abortController.abort();
    }
    this.abortController = new AbortController();

    var searchText = event.target.value;

    if (searchText.length < 2) {
      return;
    }

    /**
     * 0. split i flere ord
     *
     * Navnesøk byåsen hus
     * 1. søk etter første ord
     * 2. ta bort de treffene som ikke har resten av ordene
     *
     * Aktivitetssøk
     * 1. ?
     */

    var words = searchText.toLowerCase().split(" ");

    var orgs = await Promise.all([
      this.searchByName(words),
      this.searchByActivity(words)
    ]);

    this.addUniqueToState(flatten(orgs));
  };

  searchByName = async (searchTerms) => {
    var firstSearchTerm = searchTerms[0];
    var url = `https://organisasjoner.trondheim.kommune.no/api/organisations/v1/organisations/?name=${firstSearchTerm}`;
    var orgs = await this.fetchOrgs(url);
    var restOfSearchTerms = searchTerms.slice(1);
    var orgsThatMatchesAllSearchTerms = orgs.filter(org =>
      restOfSearchTerms.every(searchTerm => org.name.toLowerCase().includes(searchTerm))
    );
    return orgsThatMatchesAllSearchTerms;
  };

  searchByActivity = async (searchTerms) => {
    var activitySearches = flatten(searchTerms.map(term => {
      // finner alle aktiviteter som passer med søkeordet
      var activities = this.state.activity_types.filter(activity => activity.name.toLowerCase().includes(term));
      // transformerer til et objekt med id og resten av søkeordene
      return activities.map(activity => {
        return {
          id: activity.id,
          restOfSearchTerms: searchTerms.filter(t => t !== term)
        }
      })
    }))


    // konverter søkeord til liste med flere søkeresultat (Promise-liste)
    var searchResults = activitySearches.map(activitySearch => {
        var url = `https://organisasjoner.trondheim.kommune.no/api/organisations/v1/organisations/?flod_activity_type=${
          activitySearch.id
        }`;
        // 1. hent alle organisasjoner på denne aktiviteten
        // 2. ta bort organisasjoner som ikke passer på navn med de andre søkeordene

        var orgsThatMatchesAllSearchTerms = this.fetchOrgs(url)
          .then(orgs =>
            orgs.filter(org => activitySearch.restOfSearchTerms.every(searchTerm => org.name.toLowerCase().includes(searchTerm)))
          );
        return orgsThatMatchesAllSearchTerms;
    });

    var listOfResults = await Promise.all(searchResults);
    return flatten(listOfResults);
  };

  fetchOrgs(url) {
    return fetch(url, {
      signal: this.abortController.signal
    })
      .then(response => response.json())
      .then(orgs => orgs.filter(org => org.is_public !== false))
      .then(orgs => orgs.filter(this.isArtOrSport))
      .then(orgs => orgs.filter(this.isFrivilligLagEllerInnretning))
      .catch(err => {
        console.error(err);
        return [];
      });
  }

  isArtOrSport(org) {
    return (
      org.brreg_activity_code.includes("1 100") ||
      org.brreg_activity_code.includes("1 200")
    );
  }

  isFrivilligLagEllerInnretning(org) {
    return org.org_form === "FLI";
  }

  addUniqueToState = orgs => {
    orgs.sort(this.sortByName);

    var merged = [];
    orgs.forEach(org => {
      if (!merged.some(addedOrg => addedOrg.id === org.id)) {
        merged.push(org);
      }
    });

    this.setState({ organisations: merged });
  };

  sortByName(a, b) {
    if (a.name.toLowerCase() < b.name.toLowerCase()) {
      return -1; // a først
    } else if (b.name.toLowerCase() < a.name.toLowerCase()) {
      return 1; // b først
    }
    return 0; // tilfeldig rekkefølge
  }

  render() {
    var organisations = this.state.organisations.slice(0, this.state.max);
    var numberOfSearchResults = this.state.organisations.length;
    return (
      <ThemeProvider theme={theme}>

      <div>
        <TextField
          id="organisation-search"
          label="Søk etter organisasjoner"
          type="search"
          margin="normal"
          className="Organisation-TextField"
          onChange={this.handleInput}
        />
        {numberOfSearchResults > 0 &&
          numberOfSearchResults <= this.state.max && (
            <p>Viser {numberOfSearchResults} treff.</p>
          )}
        {numberOfSearchResults > this.state.max && (
          <p>
            Viser {this.state.max} av {numberOfSearchResults} treff.
          </p>
        )}
        {organisations.map(organisation => (
          <OrganisationListItem
            key={organisation.id}
            organisation={organisation}
          />
        ))}
        {numberOfSearchResults > this.state.max && (
          <div className="Organisation-VerticalSpace">
            <Button onClick={this.viewMoreSearchResults}>Vis flere</Button>
          </div>
        )}
      </div>
      </ThemeProvider>
    );
  }

  viewMoreSearchResults = () => {
    this.setState({ max: this.state.max + 20 });
  };
}

export default App;
