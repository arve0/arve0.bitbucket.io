import React from 'react';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import PropTypes from 'prop-types';


/**
 * Denne komponenten viser en organisasjon i en trekkspill-ting.
 */
class OrganisationListItem extends React.Component {
    render () {
        var organisation = this.props.organisation
        var url = `https://organisasjoner.trondheim.kommune.no${organisation.uri}`;
        var tilholdssted = organisation.tilholdssted_address && addressAsString(organisation.tilholdssted_address);

        return (
            <ExpansionPanel className="OrganisationListItem">
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography>{organisation.name}</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Typography className="OrganisationListItemInfo">
                        <b>Organisasjonsnummer: </b>{organisation.org_number}<br />
                        <If truthy={organisation.url}>
                            <b>Nettside: </b>
                            <a href={"http://" + organisation.url}>{organisation.url}</a>
                            <br />
                        </If>
                        <If truthy={organisation.phone_number}>
                            <b>Telefon: </b>
                            <a href={"tel:" + organisation.phone_number}>{organisation.phone_number}</a>
                            <br />
                        </If>
                        <If truthy={tilholdssted}>
                            <b>Tilholdssted: </b>
                            <a href={"https://maps.google.com/?q=" + tilholdssted}>{tilholdssted}</a>
                            <br />
                        </If>
                        <a href={url}>Mer informasjon om {organisation.name}</a>
                    </Typography>
                </ExpansionPanelDetails>
            </ExpansionPanel>
        );
    }
}

OrganisationListItem.propTypes = {
    organisation: PropTypes.object.isRequired
}

export default OrganisationListItem

function addressAsString(address) {
    return `${address.address_line}, ${address.postal_code} ${address.postal_city}`
}

const If = ({ truthy, children }) =>
    <span>{truthy && children}</span>