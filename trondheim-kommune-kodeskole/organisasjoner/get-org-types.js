var fetch = require("node-fetch");
var fs = require("fs");

fetch("https://organisasjoner.trondheim.kommune.no/api/organisations/v1/brreg_activity_codes/")
    .then(r => r.text())
    .then(body => fs.writeFileSync("public/org-types.json", body))
