

var verdier = [
    { navn: "Arve", id: 1 },
    { navn: "Pål", id: 2 },
    { navn: "Ingri", id: 3 },
];

<div>


<div>
    {verdier.map(verdi =>
        <p>{verdi.navn} har id {verdi.id}</p>
    )}
</div>

<div>
    {[
        <p>Arve har id 1</p>,
        <p>Pål har id 2</p>,
        <p>Ingri har id 3</p>,
    ]}
</div>

<div>
    <p>Arve har id 1</p>
    <p>Pål har id 2</p>
    <p>Ingri har id 3</p>
</div>



</div>
