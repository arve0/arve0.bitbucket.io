
import React from "react";
import PropTypes from "prop-types";

class Element extends React.Component {
    render () {
        return (
            <div>
                {this.props}
            </div>
        )
    }
}
Element.propTypes = {
    colors: PropTypes.arrayOf(PropTypes.string)
};

<Element colors={["red", "green", "blue", "yellow"]}></Element>




