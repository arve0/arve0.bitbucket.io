# Søk etter organisasjoner med jQuery

## Deployment

1. Legg all JS i en fil.
  - kopiere alt fra main.js og lib.js -> org-bundle.js

2. Kopier JS + CSS til server.
  - JS: https://arve0.bitbucket.io/trondheim-kommune-kodeskole/organisasjoner-bootstrap/org-bundle.js
  - CSS: https://arve0.bitbucket.io/trondheim-kommune-kodeskole/organisasjoner-bootstrap/main.css

3. Finn HTML som må legges til.
  - Inkludert `<script>` og `<link rel="stylesheet" ..>`, med riktig URL (steg 2).
  - HTML Kan legges til dokumentet av Javascript for å minske mengden med HTML som må kopieres.

4. Editer `mainPage` i *test.js* og kjør testene.