var state = {
  activity_types: [],
  max: 20,
  organisations: [],
  abortController: null,
};

fetch(
  `https://organisasjoner.trondheim.kommune.no/api/organisations/v1/flod_activity_types/`
)
  .then(r => r.json())
  .then(activity_types => (state.activity_types = activity_types));

$('#searchOrg').keyup(handleInput);

async function handleInput(event) {
  state.max = 20;
  state.organisations = [];
  renderState(state);

  if (state.abortController) {
    state.abortController.abort();
  }
  state.abortController = new AbortController();

  var searchText = event.target.value;

  if (searchText.length < 2) {
    return;
  }

  var words = searchText.toLowerCase().split(" ");
  var orgs = await Promise.all([searchByName(words), searchByActivity(words)]);

  addUniqueToState(flatten(orgs));
  renderState(state);
}

async function searchByName(searchTerms) {
  var firstSearchTerm = searchTerms[0];
  var url = `https://organisasjoner.trondheim.kommune.no/api/organisations/v1/organisations/?name=${firstSearchTerm}`;
  var orgs = await fetchOrgs(url);
  var restOfSearchTerms = searchTerms.slice(1);
  var orgsThatMatchesAllSearchTerms = orgs.filter(org =>
    restOfSearchTerms.every(searchTerm =>
      org.name.toLowerCase().includes(searchTerm)
    )
  );
  return orgsThatMatchesAllSearchTerms;
}

async function searchByActivity(searchTerms) {
  var activitySearches = flatten(
    searchTerms.map(term => {
      // finner alle aktiviteter som passer med søkeordet
      var activities = state.activity_types.filter(activity =>
        activity.name.toLowerCase().includes(term)
      );
      // transformerer til et objekt med id og resten av søkeordene
      return activities.map(activity => {
        return {
          id: activity.id,
          restOfSearchTerms: searchTerms.filter(t => t !== term)
        };
      });
    })
  );

  // konverter søkeord til liste med flere søkeresultat (Promise-liste)
  var searchResults = activitySearches.map(activitySearch => {
    var url = `https://organisasjoner.trondheim.kommune.no/api/organisations/v1/organisations/?flod_activity_type=${
      activitySearch.id
    }`;
    // 1. hent alle organisasjoner på denne aktiviteten
    // 2. ta bort organisasjoner som ikke passer på navn med de andre søkeordene

    var orgsThatMatchesAllSearchTerms = fetchOrgs(url).then(orgs =>
      orgs.filter(org =>
        activitySearch.restOfSearchTerms.every(searchTerm =>
          org.name.toLowerCase().includes(searchTerm)
        )
      )
    );
    return orgsThatMatchesAllSearchTerms;
  });

  var listOfResults = await Promise.all(searchResults);
  return flatten(listOfResults);
}

function fetchOrgs(url) {
  return fetch(url, {
    signal: state.abortController.signal
  })
    .then(response => response.json())
    .then(orgs => orgs.filter(org => org.is_public !== false))
    .then(orgs => orgs.filter(isArtOrSport))
    .then(orgs => orgs.filter(isFrivilligLagEllerInnretning))
    .catch(err => {
      console.error(err);
      return [];
    });
}

function isArtOrSport(org) {
  return (
    org.brreg_activity_code.includes("1 100") ||
    org.brreg_activity_code.includes("1 200")
  );
}

function isFrivilligLagEllerInnretning(org) {
  return org.org_form === "FLI";
}

function addUniqueToState(orgs) {
  orgs.sort(sortByName);

  var merged = [];
  orgs.forEach(org => {
    if (!merged.some(addedOrg => addedOrg.id === org.id)) {
      merged.push(org);
    }
  });

  state.organisations = merged;
}

function sortByName(a, b) {
  if (a.name.toLowerCase() < b.name.toLowerCase()) {
    return -1; // a først
  } else if (b.name.toLowerCase() < a.name.toLowerCase()) {
    return 1; // b først
  }
  return 0; // tilfeldig rekkefølge
}
