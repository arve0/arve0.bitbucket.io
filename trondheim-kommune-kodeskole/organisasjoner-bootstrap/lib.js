function flatten(list, depth) {
  depth = (typeof depth == 'number') ? depth : Infinity;

  if (!depth) {
    if (Array.isArray(list)) {
      return list.map(function(i) { return i; });
    }
    return list;
  }

  return _flatten(list, 1);

  function _flatten(list, d) {
    return list.reduce(function (acc, item) {
      if (Array.isArray(item) && d < depth) {
        return acc.concat(_flatten(item, d + 1));
      }
      else {
        return acc.concat(item);
      }
    }, []);
  }
};

function renderState(state) {
  renderSearchResults(state.organisations)
}

function renderSearchResults(organisations) {
  $('#searchResult').empty();

  organisations.forEach((org, i) => {
    $(`
    <div class="card">
      <div class="card-header" id="heading${i}">
        <h2 class="mb-0">
          <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse${i}" aria-expanded="false" aria-controls="collapse${i}">
            ${org.name}
          </button>
        </h2>
      </div>
      <div id="collapse${i}" class="collapse" aria-labelledby="heading${i}" data-parent="#searchResult">
        <div class="card-body">
          <h3>Organisasjonsnummer</h3>${org.org_number}<br />
          ${addUrlIfExists(org.url)}
          ${addPhoneIfExists(org.phone_number)}
          ${addAddressIfExists(org.tilholdssted_address)}
        </div>
      </div>
    </div>`).appendTo('#searchResult')
  })
}

function addUrlIfExists(url) {
  if (url) {
    return `<h3>Nettside</h3><a href="http://${url}">${url}</a>`
  } else {
    return ``
  }
}

function addPhoneIfExists(number) {
  if (number) {
    return `<h3>Telefon</h3><a href="tel:${number}">${number}</a>`
  } else {
    return ``
  }
}

function addAddressIfExists(address) {
  if (address) {
    let addressText = createAddressText(address)
    return `<h3>Tilholdssted</h3>
      <a href="https://maps.google.com/?q=${addressText}">
        ${addressText}
      </a>`;
  } else {
    return ``;
  }
}

function createAddressText(address) {
  return `${address.address_line}, ${address.postal_code} ${address.postal_city}`
}
