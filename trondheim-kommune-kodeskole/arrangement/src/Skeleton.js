import React from "react";

export default () => (
    <div>
    <p class="skeleton-text">Laster ...</p>
    <div class="skeleton-container">
      <div class="skeleton-card">
        <div>
          <div class="skeleton-card-img"></div>
          <div class="skeleton-card-content">
            <h2>______ _____ __ _____ ___</h2>
            <ul>
              <li>______ __. ________</li>
              <li>__:__ - __:__</li>
              <li>____, ________</li>
              <li>____</li>
              <li>______</li>
            </ul>
          </div>
        </div>
      </div>
      <div class="skeleton-card">
        <div>
          <div class="skeleton-card-img"></div>
          <div class="skeleton-card-content">
            <h2>______ _____ __ _____ ___</h2>
            <ul>
              <li>______ __. ________</li>
              <li>__:__ - __:__</li>
              <li>____, ________</li>
              <li>____</li>
              <li>______</li>
            </ul>
          </div>
        </div>
      </div>
      <div class="skeleton-card">
        <div>
          <div class="skeleton-card-img"></div>
          <div class="skeleton-card-content">
            <h2>______ _____ __ _____ ___</h2>
            <ul>
              <li>______ __. ________</li>
              <li>__:__ - __:__</li>
              <li>____, ________</li>
              <li>____</li>
              <li>______</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    </div>
)