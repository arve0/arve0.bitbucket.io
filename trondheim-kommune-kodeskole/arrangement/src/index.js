 /*eslint-env jquery*/

import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

// fjern stil fra template som ødelegger stil til eksempelvis button
if (window.jQuery) {
    jQuery(".bd-tagstyles").removeClass("bd-tagstyles")
}

ReactDOM.render(<App />, document.getElementById('tk-arrangement'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
