import React from "react";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Typography,
  IconButton
} from "@material-ui/core";
import {
  Schedule,
  Category,
  Event,
  Launch,
  MyLocation,
  Payment,
  Close
} from "@material-ui/icons";
import Kategorier from "./kategorier.js";
import If from "./If.js";
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500]
  }
});

class EventDialog extends React.Component {
  render() {
    const { classes } = this.props;
    const { open } = this.props;
    const { lukk } = this.props;
    const { event } = this.props;
    const start = new Date(event.startDate);

    const prettyStart = pretty(start);
    const end = new Date(event.endDate);

    let prettyEnd = "";

    // const { classes } = this.props;
    let pris = event.regularPrice || "Gratis";
    if (pris !== "Gratis") {
      pris += " kroner";
    }

    if (event.duration !== "") {
      prettyEnd = end.toLocaleTimeString("nb-NO", {
        hour: "numeric",
        minute: "numeric"
      });

      const days = [
        "søndag ",
        "mandag ",
        "tirsdag ",
        "onsdag ",
        "torsdag ",
        "fredag ",
        "lørdag "
      ];

      if (start.getDate() !== end.getDate()) {
        prettyEnd = days[end.getDay()] + prettyEnd;
      }
      prettyEnd = " - " + prettyEnd;
    }

    let now = new Date();
    let repetitions = event.repetitions.filter(repetition => {
      let date = makeDateOfRepetition(repetition);
      return date >= now;
    });

    return (
      <Dialog open={open} onClose={lukk}>
        <DialogTitle>
          {event.title_nb}
          <IconButton aria-label="close" className={classes.closeButton} onClick={lukk}>
            <Close />
          </IconButton>
        </DialogTitle>
        <DialogContent>
          <img src={event.imageURL} alt="eventbilde" /> <br />
          {event.desc_nb.split(/\n+/).map(tekst => (
            <Typography paragraph={true}>{tekst}</Typography>
          ))}
          <List dense={true}>
            <ListItem>
              <ListItemIcon>
                <MyLocation />
              </ListItemIcon>{" "}
              <ListItemText>{event.venueObj.name}</ListItemText>
            </ListItem>
            <ListItem>
              <ListItemIcon>
                <Event />
              </ListItemIcon>{" "}
              <ListItemText> {prettyStart}</ListItemText>
            </ListItem>
            <ListItem alt="klokkeikon">
              <ListItemIcon>
                <Schedule />
              </ListItemIcon>{" "}
              <ListItemText>
                {" "}
                klokka {event.startTime} {prettyEnd}
              </ListItemText>
            </ListItem>
            <ListItem>
              <ListItemIcon>
                <Category />
              </ListItemIcon>{" "}
              <ListItemText>
                {event.categories.map(c => Kategorier[c]).join(", ")}
              </ListItemText>
            </ListItem>
            <ListItem>
              <ListItemIcon>
                <Payment />
              </ListItemIcon>{" "}
              <ListItemText>{pris}</ListItemText>
            </ListItem>
          </List>
          <If truthy={repetitions.length >= 1}>
            <Typography variant="h5">Gjentakelser</Typography>
            <List dense={true}>
              {repetitions.map((repetition, i) => (
                <ListItem alt="klokkeikon" key={i}>
                  <ListItemIcon>
                    <Schedule />
                  </ListItemIcon>
                  <ListItemText>
                    {pretty(makeDateOfRepetition(repetition))}
                    klokken {repetition.startTime}
                  </ListItemText>
                </ListItem>
              ))}
            </List>
          </If>
          <Typography paragraph={true}>
            <a href={event.eventLink} target="0" title={event.title_nb}>
              Vis arrangementet i eget vindu <Launch />
            </a>
          </Typography>
          <Typography>Adresse: </Typography>
          <Typography>
            <a
              href={"https://maps.google.com/?q=" + event.venueObj.address}
              target="0"
            >
              {" "}
              {event.venueObj.address}
              <img src={event.venueObj.mapImageURL} alt="kartvisning" />
            </a>
          </Typography>
        </DialogContent>
      </Dialog>
    );
  }
}

export default withStyles(styles)(EventDialog);

function pretty(date) {
  return (
    date.toLocaleDateString("nb-NO", {
      weekday: "long",
      // year: 'numeric',
      month: "long",
      day: "numeric"
    }) + " "
  );
}

/**
 * {
 *  startDate: {_seconds: 1568152800, _nanoseconds: 0},
 *  startDateRep: "2019-09-10T22:00:00.000+00:00",
 *  startTime: "10:30",
 * }
 *
 * @param {*} repetition
 */
function makeDateOfRepetition(repetition) {
  const day = new Date(repetition.startDateRep);
  const [hour, minutes] = repetition.startTime.split(":");
  return new Date(
    day.getFullYear(),
    day.getMonth(),
    day.getDate(),
    hour,
    minutes
  );
}
