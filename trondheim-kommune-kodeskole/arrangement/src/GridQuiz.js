import React from "react";
import Grid from "@material-ui/core/Grid";
import { withStyles } from '@material-ui/core/styles';

class GridQuiz extends React.Component {
  render() {
    const style = {
      backgroundColor: 'lightgreen',
      border: '1px solid',
      padding: '1vw',
      height: '100vh',
      textAlign: 'center'
    }

    return (
      <Grid container>
        <Grid item md={6} style={style}>A</Grid>
        <Grid item md={3} style={style}>B</Grid>
        <Grid item md={2} style={style}>C</Grid>
        <Grid item md={1} style={style}>D</Grid>
      </Grid>
    );
  }
}

export default withStyles()(GridQuiz)