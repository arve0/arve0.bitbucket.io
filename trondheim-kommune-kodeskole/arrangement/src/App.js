import React from "react";
import "./App.css";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import EventItem from "./EventItem";
import Button from "@material-ui/core/Button";
import { createMuiTheme, Typography } from "@material-ui/core";
import { ThemeProvider } from "@material-ui/styles";
import Skeleton from "./Skeleton.js";
import If from "./If.js";

const theme = createMuiTheme({
  typography: {
    // Bootstrap har 10px som standardstørrelse, mens Material-UI sin standardstørrelse er 16px
    htmlFontSize: 10
  }
});

const styles = theme => ({
  root: {
    flexGrow: 1
  }
});

class App extends React.Component {
  state = {
    events: [],
    max: 20,
    loading: true
  };

  constructor(props) {
    super(props);
    // fetch("https://us-central1-trdevents-224613.cloudfunctions.net/getNextEvents?category=CONCERT")
    fetch("https://us-central1-tk-events.cloudfunctions.net/getNextEvents")
      .then(r => r.json())
      // .then(isTrondheimFolkebibliotek)
      .then(r => this.setState({ events: r, loading: false }));
  }

  handleChange = key => (event, value) => {
    this.setState({
      [key]: value
    });
  };

  render() {
    const { classes } = this.props;
    const events = this.state.events.slice(0, this.state.max);
    const numberOfResults = this.state.events.length;

    return (
      <ThemeProvider theme={theme}>
        <If truthy={this.state.loading}>
          <Skeleton />
        </If>
        <If truthy={!this.state.loading}>
          <div className="tk-arrangement-root">
            {numberOfResults > 0 && numberOfResults <= this.state.max && (
              <Typography>Viser {numberOfResults} treff.</Typography>
            )}
            {numberOfResults > this.state.max && (
              <Typography>
                Viser {this.state.max} av {numberOfResults} treff.
              </Typography>
            )}

            <Grid container className={classes.root} spacing={2}>
              {events.map((event, i) => (
                <Grid key={i} item md={4} sm={6} xs={12}>
                  <EventItem event={event} />
                </Grid>
              ))}
            </Grid>
            {numberOfResults > this.state.max && (
              <div className="Organisation-VerticalSpace">
                <Button
                  onClick={() => this.setState({ max: this.state.max + 20 })}
                >
                  Vis flere
                </Button>
              </div>
            )}
          </div>
        </If>
      </ThemeProvider>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(App);

// function isTrondheimFolkebibliotek(arrangement) {
//   return arrangement.filter(a => {
//     return a.venueObj.name.toLowerCase().includes("trondheim folkebibliotek");
//   });
// }
