import React from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import CardMedia from "@material-ui/core/CardMedia";
import Schedule from "@material-ui/icons/Schedule";
import EventIcon from "@material-ui/icons/Event";
import MyLocation from "@material-ui/icons/MyLocation";
import Category from "@material-ui/icons/Category";
import Payment from "@material-ui/icons/Payment";
import {
  CardActionArea,
  List,
  ListItem,
  ListItemIcon,
  ListItemText
} from "@material-ui/core";
import Categories from "./kategorier.js";
import EventDialog from "./EventDialog.js";

class EventItem extends React.Component {
  state = {
    kartErÅpent: false
  };

  render() {
    const event = this.props.event;
    const start = new Date(event.startDate);
    let prettyStart = pretty(start);

    if (event.repetitions.length > 0) {
        prettyStart += " (gjentagende)"
    }

    const end = new Date(event.endDate);

    let prettyEnd = "";

    if (event.duration !== "") {
      prettyEnd = end.toLocaleTimeString("nb-NO", {
        hour: "numeric",
        minute: "numeric"
      });

      const days = [
        "søndag ",
        "mandag ",
        "tirsdag ",
        "onsdag ",
        "torsdag ",
        "fredag ",
        "lørdag "
      ];

      if (start.getDate() !== end.getDate()) {
        prettyEnd = days[end.getDay()] + prettyEnd;
      }

      prettyEnd = " - " + prettyEnd;
    }

    const { classes } = this.props;

    let pris = event.regularPrice || "Gratis";
    if (pris !== "Gratis") {
      pris += " kroner";
    }

    const åpneKart = () => {
      this.setState({ kartErÅpent: true });
    };
    const lukkKart = () => {
      this.setState({ kartErÅpent: false });
    };

    return (
      <Card className={classes.kort}>
        <EventDialog
          open={this.state.kartErÅpent}
          lukk={lukkKart}
          event={event}
        />
        <CardActionArea onClick={åpneKart}>
          <CardMedia className={classes.media} image={event.imageURL} />
          <CardContent>
            <Typography variant="h5" component="h2">
              {event.title_nb}
            </Typography>
            <List dense={true}>
              {/* dato */}
              <ListItem className={classes.details}>
                <ListItemIcon className={classes.icon}>
                  <EventIcon alt="Dato" />
                </ListItemIcon>
                <ListItemText>{prettyStart}</ListItemText>
              </ListItem>

              {/* tid  */}
              <ListItem className={classes.details}>
                <ListItemIcon className={classes.icon}>
                  <Schedule />
                </ListItemIcon>
                <ListItemText>
                  {event.startTime} {prettyEnd}
                </ListItemText>
              </ListItem>

              {/* Sted */}
              <ListItem className={classes.sted}>
                <ListItemIcon className={classes.icon} onClick={åpneKart}>
                  <MyLocation />
                </ListItemIcon>
                <ListItemText onClick={åpneKart}>
                  {event.venueObj.name}
                </ListItemText>
              </ListItem>

              <ListItem className={classes.details}>
                <ListItemIcon className={classes.icon}>
                  <Category />
                </ListItemIcon>
                <ListItemText>
                  {event.categories.map(c => Categories[c]).join(", ")}
                </ListItemText>
              </ListItem>
              <ListItem className={classes.details}>
                <ListItemIcon className={classes.icon}>
                  <Payment />
                </ListItemIcon>
                <ListItemText>{pris}</ListItemText>
              </ListItem>
            </List>
          </CardContent>
        </CardActionArea>
      </Card>
    );
  }
}

const styles = {
  kort: {
    height: "100%"
  },
  media: {
    height: 200
  },
  details: {
    paddingLeft: 0,
    paddingRight: 0
  },
  sted: {
    paddingLeft: 0,
    paddingRight: 0,
    cursor: "pointer"
  },
  icon: {
    minWidth: "40px"
  }
};

export default withStyles(styles)(EventItem);

function pretty(date) {
  return date.toLocaleDateString("nb-NO", {
    weekday: "long",
    // year: "numeric",
    month: "long",
    day: "numeric"
  });
}
