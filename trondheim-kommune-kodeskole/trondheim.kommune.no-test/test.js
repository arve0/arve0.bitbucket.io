const configuration = require('./package.json')
const puppeteer = configuration.devDependencies.puppeteer
    ? require('puppeteer')
    : require('puppeteer-firefox');
const assert = require('assert')

// configuration
const mainPage = `https://wwwqa.trondheim.kommune.no/`
const headless = false  // false: show browser, true: hide browser
const slowMo = true  // true: each browser action will take 100 milliseconds
    ? 100
    : 0

// globals
let browser = null
let page = null

before(async function () {
    this.timeout(10 * 1000) // starting browser may take more than 2 seconds

    browser = await puppeteer.launch({ headless, slowMo })
    page = (await browser.pages())[0]

    // page.on('console', async function (msg) {
    //     if (msg.type() === 'error' && msg.args().length) {
    //         let args = await Promise.all(msg.args().map(arg => arg.jsonValue()))
    //         console.error("Browser console.error:", ...args)
    //     } else {
    //         console.log(msg._text)
    //     }
    // })
})

beforeEach(async function () {
    this.timeout(5 * 1000)
    await page.goto("https://biblioteket.trondheim.kommune.no")
})

after(function () {
    browser.close()
})

describe('skrivebord', function () {
    this.timeout(slowMo === 0 ? 4000 : 0)

    before(() => {
        page.setViewport({
            width: 1280,
            height: 1024,
        })
    })

    it("Åpningstider", async function(){
        await waitFor({ text: "Sentrum", selector: "a", click: true});
        await waitFor({ text: "Mandag" })
        await waitFor({ text: "Lukk", click: true });
    });

    it("kalender", async function() {
        await waitFor({ selector: '#container > .list-by-date a[href="skip"]', click: true })
        await page.waitForNavigation()
        const url = await page.url()
        assert(url.includes('kalender'), 'URL inneholder ikke ordet kalender etter klikk på første arrangement.')
    })

    //     it('søk etter barnehage', async function () {
    //     for (let i = 0; i < 8; i++) {
    //         await page.keyboard.press('Tab')
    //     }
    //     await page.keyboard.type('barnehage')
    //     await waitFor({ text: 'Søk', click: true })
    //     await waitFor({ text: 'Søk om plass, endre oppholdstid' })
    // })

    // it('sende inn fant du', async function () {
    //     await waitFor({ text: 'Bygg, kart og eiendom', click: true })
    //     await waitFor({ text: 'Prisliste for plan- og bygningstjenester', click: true })
    //     await waitFor({ selector: 'input[value="Nei"]', click: true })
    //     await page.keyboard.press('Tab')
    //     await page.keyboard.type('bare humbug bare humbug bare humbug bare humbug')
    //     // await waitFor({ text: 'Hva forsøkte du å finne?' })
    //     // let input = await page.$('textarea')
    //     // await input.type('bare humbug bare humbug bare humbug bare humbug')
    //     await waitFor({ selector: 'input[value="Send tilbakemelding"]', click: true })
    //     await waitFor({ text: 'Takk for din tilbakemelding', timeout: 10000 })
    // })

    // it('sende inn fant du', async function () {
    //     await waitFor({ text: 'Bygg, kart og eiendom', click: true })
    //     await waitFor({ text: 'Prisliste for plan- og bygningstjenester', click: true })
    //     await waitFor({ text: 'Priser - plan- og bygningstjenester' })
    //     await waitFor({ selector: 'input[value="Nei"]', click: true })
    //     await waitFor({ selector: 'textarea', focus: true })
    //     await page.keyboard.type('bare humbug bare humbug bare humbug bare humbug')
    //     await waitFor({ selector: 'input[value="Send tilbakemelding"]', click: true })
    //     await waitFor({ text: 'Takk for din tilbakemelding', timeout: 10000 })
    // })
})

/**
 * Waits for a visible element containing given text, possibly clicks it.
 *
 * @param {object} params - What to wait for, in which selector, if it should be clicked and how long to wait.
 * @param {string} params.text - What text to wait for.
 * @param {string} params.selector - What selector to use, defaults to any element: `*`.
 * @param {bool} params.click - Wheter to click when found.
 * @param {number} params.timeout - How long to wait in milliseconds.
 */
async function waitFor({ text = '', selector = '*', click = false, focus = false, timeout = 5000 }) {
    const start = Date.now()

    while ((Date.now() - start) < timeout) {
        let frames = await page.frames()
        let scopes = [page, ...frames]
        for (let scope of scopes) {
            let result
            try {
                result = await scope.evaluate(pageFunction, text, selector, click)
            } catch (err) {
                // probably lost execution context, break and get new scopes
                break
            }
            if (result) {
                return true
            }
        }
    }

    throw new Error(`'${text}' not found on page in selector '${selector}', waited ${timeout} milliseconds.`)

    function pageFunction (text, selector, click) {
        let match = findElement(text, selector)

        if (match) {
            if (click) {
                match.click()
            }
            if (focus) {
                match.focus()
            }
            return true
        }
        return false

        function findElement(text, selector) {
            let matchingElements = Array.from(document.querySelectorAll(selector))
                .filter(element => element.textContent.includes(text))
                .sort(shortestTextVisibleIfSameLength) // shortest text first, e.g. "best" search result

            if (matchingElements.length > 0 && isVisible(matchingElements[0])) {
                return matchingElements[0]
            }

            let shadowedElements = Array.from(document.querySelectorAll(selector))
                .filter(element => element.shadowRoot)
                .flatMap(element => Array.from(element.shadowRoot.querySelectorAll(selector)))
                .filter(element => element.textContent.includes(text))
                .sort(shortestTextVisibleIfSameLength)

            if (shadowedElements.length > 0 && isVisible(shadowedElements[0])) {
                return shadowedElements[0]
            }

            return null
        }

        function shortestTextVisibleIfSameLength(a, b) {
            let difference = a.textContent.length - b.textContent.length;
            if (difference === 0) {
                let aVisible = isVisible(a)
                let bVisible = isVisible(b)

                if (aVisible && !bVisible) {
                    return -1
                } else if (!aVisible && bVisible) {
                    return 1
                } else {
                    return 0
                }
            }
            return difference
        }

        function isVisible(element) {
            return element.offsetParent !== null
        }
    }
}