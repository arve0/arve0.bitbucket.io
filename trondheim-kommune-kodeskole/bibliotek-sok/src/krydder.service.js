/**
 * Bruk krydder.bib.no til å hente bilder for bøker.
 */

const CORS = "https://cors.seljebu.no/";
const BASE = CORS + "https://krydder.bib.no/cgi-bin/krydderxml?bibid=";

export function image_url(bibliofilid) {
    return fetch(BASE + bibliofilid)
        .then(r => r.text())
        .then(get_img_url_in_xml)
}

function get_img_url_in_xml(xml) {
    const doc = new DOMParser().parseFromString(xml, "text/xml");
    const images = Array.from(doc.querySelectorAll("bildedata"));
    const image = images.find(img => {
        return get_inner(img, "størrelse") === "256x";
    });
    if (!image) {
        return "";
    }
    return get_inner(image, "url");
}

function get_inner(element, selector) {
    let inner = element.querySelector(selector);
    if (!inner) {
        return ""
    }
    return inner.innerHTML;
}
