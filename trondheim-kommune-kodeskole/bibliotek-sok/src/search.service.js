/**
 * Søk etter bøker i følgende rekkefølge:
 *
 * - dc.title="verbatim søketekst"
 * - dc.creator="verbatim søketekst"
 * - dc.title=verbatim and dc.title=søketekst
 * - dc.creator=verbatim and dc.creator=søketekst
 * - cql.anywhere="verbatim søketekst"
 * - cql.anywhere=verbatim and cql.anywhere=søketekst
 *
 * Endepunkt: https://www.tfb.no/cgi-bin/sru
 * Query:
 *  operation=searchRetrieve
 *  recordSchema=bsdc
 *  query=søket (dc.title, dc.creator, cql.anywhere)
 */
import { writable } from 'svelte/store';

// const debouncedSearch = debounce(performSearch, 300);

const MAXIMUM_RECORDS = 100;
const CORS = "https://cors.seljebu.no/";
// const BASE = CORS + "https://www.tfb.no/cgi-bin/sru?operation=searchRetrieve&recordSchema=bsdc&query=";
const BASE = CORS + "https://trondheim.bib.no/cgi-bin/sru?"
    + "operation=searchRetrieve" 
    + `&maximumRecords=${MAXIMUM_RECORDS}`
    + "&sortKeys=date,,false" 
    + "&recordSchema=bsdc" 
    + "&query=";

const { subscribe, set, update } = writable([]);

export default {
    subscribe,
    perform: search,
}

let previous_search = '';
let previous_promise = null;
let previous_context = { abort: false };

export function search(text) {
    console.log(`searching for '${text}'`)
    let normalized = text.trim()
        .replace(/ +/g, ' ')
        .replace(/[?&="]/g, '')
        .toLowerCase();

    if (normalized === "") {
        return Promise.resolve([]);
    } else if (normalized === previous_search) {
        return previous_promise;
    }

    previous_context.abort = true;
    // create new object that is sent to current request
    previous_context = { abort: false };
    previous_search = normalized;
    previous_promise = perform_search(normalized, previous_context);

    console.log("returning promise");

    return previous_promise;
}

async function perform_search(text, context) {
    // debounce, send requests every 500 ms
    await wait(500);
    if (context.abort) {
        throw new Error('search aborted');
    }

    console.log(`performing search for '${text}'`)

    let results = [];

    let strategies = [
        creator_verbatim,
        creator_includes_all_words,
        title_verbatim,
        title_includes_all_words,
        anywhere_verbatim,
        anywhere_includes_all_words,
    ]

    for (let strategy of strategies) {
        if (context.abort) {
            throw new Error('search aborted');
        }
        let new_results = await strategy(text)
            .then(r => r.text())
            .then(xml_to_json)
            .then(r => {
                console.log(`${r.length} hits`)
                return r
            });

        add_missing(results, new_results);

        if (results.length >= MAXIMUM_RECORDS) {
            break;
        }
    }

    // maximum 10 results
    results = results
        // .slice(0, MAXIMUM_RECORDS)
        // .filter(r => r.language === "nob")
        .map(r => {
            // let img = image_url(r.bibliofilid);
            let score = calculate_score(r, text);
            return { ...r, score };
        });

    results.sort((a, b) => b.score - a.score);

    await wait(1);
    if (context.abort) {
        throw new Error('search aborted');
    }
    console.log("returning results", results);

    return results;
}

function wait (timeout) {
    return new Promise(resolve => {
        setTimeout(resolve, timeout)
    })
}

function title_verbatim(text) {
    console.log(`title_verbatim: ${text}`)
    if (text.length <= 2) {
        Promise.resolve([]);
    }
    return fetch(BASE + `dc.title="${text}*"`);
}

function creator_verbatim(text) {
    console.log(`creator_verbatim: ${text}`)
    if (text.length <= 2) {
        Promise.resolve([]);
    }
    return fetch(BASE + `dc.creator="${text}*"`);
}

function title_includes_all_words(text) {
    console.log(`title_includes_all_words: ${text}`)
    let query = text.split(" ")
        .filter(w => w.length > 2)
        .map(w => `dc.title=${w}*`)
        .join(" and ");

    if (query === "") {
        Promise.resolve([]);
    }

    return fetch(BASE + query);
}

function creator_includes_all_words(text) {
    console.log(`creator_includes_all_words: ${text}`)
    let query = text.split(" ")
        .filter(w => w.length > 2)
        .map(w => `dc.creator=${w}*`)
        .join(" and ");
    if (query === "") {
        Promise.resolve([]);
    }
    return fetch(BASE + query);
}

function anywhere_verbatim(text) {
    console.log(`anywhere_verbatim: ${text}`)
    if (text.length <= 2) {
        Promise.resolve([]);
    }
    return fetch(BASE + `cql.anywhere="${text}*"`);
}

function anywhere_includes_all_words(text) {
    console.log(`anywhere_includes_all_words: ${text}`)
    let query = text.split(" ")
        .filter(w => w.length > 2)
        .map(w => `cql.anywhere=${w}*`)
        .join(" and ");

    if (query === "") {
        Promise.resolve([]);
    }

    return fetch(BASE + query);
}

async function xml_to_json(xml) {
    const doc = new DOMParser().parseFromString(xml, "text/xml");
    let results = [];

    for (let record of doc.querySelectorAll("record")) {
        let title = get_inner(record, "title");
        let creator = firstname_lastname(get_inner(record, "creator"));
        let img = get_inner(record, "krydderbildeurl");
        let description = get_inner(record, "description");
        let publisher = get_inner(record, "publisher");
        let date = get_inner(record, "date");
        let format = get_inner(record, "format");
        let isbn = get_inner(record, "#isbn");
        let localid = get_inner(record, "#localid");
        let bibliofilid = get_inner(record, "#bibliofilid");
        let hylle = get_inner(record, "#hylle");
        let language = get_inner(record, "language");

        results.push({
            title, creator, img, description, publisher, date, format,
            isbn, localid, bibliofilid, hylle, language
        });
    }

    return results;
}

function get_inner(element, selector) {
    let inner = element.querySelector(selector);
    if (!inner) {
        return ""
    }
    return inner.innerHTML;
}

function firstname_lastname(name) {
    if (name.includes(",")) {
        let [last, ...rest] = name.split(",");
        return rest.map(n => n.trim()).join(" ") + " " + last.trim();
    } else {
        return name;
    }
}

function add_missing(results, new_results) {
    let count = 0;
    new_results.forEach(r => {
        let is_missing = results.every(rr => {
            let id_different = r.localid !== rr.localid;
            let isbn_different = r.isbn !== "" 
                ? r.isbn !== rr.isbn
                : true;
            console.log(id_different, isbn_different)
            return id_different && isbn_different;
        });
        if (is_missing) {
            results.push(r);
            count += 1;
            console.log(`Added entry, id=${r.localid} isbn=${r.isbn} title='${r.title}'`);
        } else {
            console.log(`Duplicate entry, id=${r.localid} isbn=${r.isbn} title='${r.title}'`);
        }
    });

    console.log(`added ${count} of ${new_results.length}, ${results.length} hits in total`);
}

function calculate_score(result, text) {
    let score = 0;
    score += score_for_field(result.creator, text, 1000);
    console.log(`score '${result.title} (${result.format}, ${result.date})'`)
    console.log(`creator: ${score}`)

    score += score_for_field(result.title, text, 800);
    console.log(`title: ${score}`)

    // new -> higher score
    score += parseInt(result.date) / 1000;
    console.log(`year: ${score}`)

    // language
    // nob > mul > other
    score += result.language.toLowerCase() === "nob" ? 50 : 0;
    score += result.language.toLowerCase() === "mul" ? 40 : 0;

    // by category
    // book -> lyd -> other
    let format = result.format.toLowerCase();
    score += format.includes("bok") ? 30 : 0;
    score += format.includes("blu-ray") ? 25 : 0;
    score += format.includes("dvd")? 20 : 0;
    score += format.includes("lyd") ? 10 : 0;
    console.log(`category: ${score}`)

    return score;
}

function score_for_field(field, text, multiplier) {
    field = field.toLowerCase();
    if (!field || field === "") {
        return 0;
    }
    let score = 0;
    if (field === text) {
        score += 10;
    } else if (field.includes(text)) {
        score += 9;
    } else if (all_words_in_text(field, text)) {
        score += 8;
    } else {
        score += word_count(field, text);
    }

    return score * multiplier;
}

function all_words_in_text(text, words) {
   return words.split(" ").every(w => text.includes(w));
}

function word_count(text, words) {
    let count = words.split(" ")
        .filter(w => w.length > 2)
        .reduce((count, w) => count + (text.indexOf(w) === 0 ? 1 : 0), 0);

    return Math.min(7, count);
 }