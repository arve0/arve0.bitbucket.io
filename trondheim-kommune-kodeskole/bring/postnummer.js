
var postNummerElement = document.querySelector("input#postnummer")
var postStedElement = document.querySelector("input#poststed")

function hentPostSted () {
    var postNummer = postNummerElement.value
    var clientUrl = location.href
    var URL = `https://api.bring.com/shippingguide/api/postalCode.json?clientUrl=${clientUrl}&pnr=${postNummer}`

    fetch(URL).then(response => response.json())
        .then(function (fraBring) {
            // oppdater dokumentet
            postStedElement.value = fraBring.result
            console.log(fraBring)
        })
}

postNummerElement.addEventListener("input", hentPostSted)