

/*
Hvilken er *ikke* en gyldig funksjon?
Hvilken funksjon heter stine?
Hva returnerer arve(2)?
Hvilken funksjon returnerer undefined?
*/

function arve (x) {
    return x + 1
}

var stine = (x) => x + 2

var pal = function (x) { x + 3 }

//fn ingri (x) { return x + 4 }


/*
Er variabelen arve et tall?
Hvilken datatype har variabelen stine?
Er variabelen pal et objekt?
Hvilken datatype har variabelen ingrid?
Hva er verdien til ingri.a?
Hva er verdien til ingri.b?
*/

var arve = 1
var stine = "qwerty"
var pal = true
var ingri = { a: 123 }


/*
Hva er verdien til variabelen tekst?
*/

var a = 45
var tekst = `Tallet er ${a}`

/*
Hva vil koden skrive ut?
Hva verdien til object.a[1]?
*/

var objekt = {
    a: [1, 2, 3],
    b: "asdf",
    c: true
}

for (var key in objekt) {
    console.log(key)
}

/*
Hva vil koden skrive ut?
Hvordan kan lage JSON av variabelen objekt? JSON.stringify(), JSON.parse()
*/


var objekt = {
    a: [1, 2, 3],
    b: "asdf",
    c: true
}

for (var val in Object.values(objekt)) {
    console.log(val)
}

/*
Hva returnerer fetch?
HTTP
Hvilken metode bruker vi for å inspisere responsen?
*/

fetch(url)
    ....



/*
Hvilket element vil koden hente?
*/

var input = document.querySelector('input#asdf')


/*
Når vil koden under kjøre?
Hva skjer med mellomrom i input-elementet ved keyup?
*/

input.addEventListener('keyup', () => {
    input.value = input.value.replace(' ', '.')
})


/*
Hva vil koden skrive ut?
*/

var a = true
if (a) {
    console.log("a er sann")
} else {
    console.log("a er _ikke_ sann")
}