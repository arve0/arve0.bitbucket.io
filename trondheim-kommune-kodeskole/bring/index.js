// Funksjoner: navn, parameter/argument, returverdi

function utenParametre () {
    return 4
}

function arve (x) {
    return x + 1
}

//        navn       parameter
function gjørNoeLurt (x, y) {
    // funksjonsblokk

    return x + y
}

function denBestePostKoden () {
    document.querySelector("input#postNummer").value = "9100"
}  // returnerer undefined

denBestePostKoden()


// kort notasjon, "lambda"

var sammeSomArveOver = (x) => x + 1
//    kan også skrives  x => x + 1  når det kun er en parameter


// Bruke funksjoner: over + console.log

arve(111)

console.log("tekst som sendes inn")


// Variabler: navn/verdi, typer (number, string, bool, object)

var petter = 123  // <-- tall
var ida = "tekst" // <-- string
var ola = true    // <-- boolean
var ola2 = false  // <-- boolean
var maria = {     // <-- object
    a: 123,       // a er nøkkel, 123 verdien
    b: true,      // b er nøkkel, true er verdi
    c: "tekst",   // c er nøkkel, "tekst" er verdi
    d: null,      // nesten lik undefined
    e: {
        a: 1,     // maria.e.a er lik 1
    }
}


// Objekter: keys/values

maria.c // for å få tak i nøkkel c med verdi "tekst"
maria.d // undefined, fordi nøkkelen ikke eksisterer


// Kombinere to variabler i en tekst - template strings

var postNummer = 7040
var URL = `https://api.bring.com/shippingguide/api/postalCode.json?clientUrl=asdf&pnr=${postNummer}`


// DOM: document.querySelector, addEventListener input, input.value
// Document Object Model - et fint navn for HTML

var postNummerElement = document.querySelector("input#postNummer")  // "input#postNummer" er <input id="postNummer"> i HTML
// Annen vanlig brukt selektor er "input.red" = <input class="red">


// Hendelse - ofte noe brukeren gjør på siden, men kan også være ting som "bildet er lastet ned"

postNummerElement.addEventListener("input", function lytter () {

    console.log(postNummerElement.value)

    // Kontrollstrukturer: if, else

    if (postNummerElement.value.length === 4) {
        console.log("Postnummer er kanskje korrekt.")
    } else {
        console.log("Postnummer er _ikke_ korrekt.")
    }
})

