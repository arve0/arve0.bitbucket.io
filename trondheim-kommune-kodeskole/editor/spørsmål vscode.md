1. Hvor finner en instillingene til VSCode?
2. Hva er forskjell på snippet og emmet?
3. Hva vil nav.topp-meny autofullføre til?
4. Hvor kan du legge til egne maler?
5. Hva betyr `$1` i malen?
6. Hva betyr `$0` i malen?
7. Hvor mange linjer består følgende mal av?

   ```json
   "named function": {
   	"prefix": "fn",
   	"body": [
   		"function ${1:name}(${2:argument}) {",
   		"    $3",
   		"}",
   		"$0",
   	],
   	"description": "named function with argument"
   }
   ```
8. Hvor stopper markøren i malen "named function"?
9. Hva gjør "Prettier"-tillegget?
10. Det er i hovedsak to typer autofullføringer i JS, hva er forskjellen på de?
11. Hvor kan man endre tastatursnarveier?
12. Hvilken tastatursnarvei åpner filer med fuzzy-find?
13. Hvilken tastatursnarvei finner menyer med fuzzy-find?