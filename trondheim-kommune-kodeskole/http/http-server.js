/**
 * Test denne HTTP-serveren med `curl -v localhost:3000`,
 * bruk deretter netcat til det samme.
 */

const express = require('express')
const cookieParser = require('cookie-parser')
const app = express()

app.use(cookieParser())
app.use((request, response) => {
    console.log('Fikk forespørsel:', request.url)
    console.log('Fikk headers:')
    console.dir(request.headers)

    response.status = 200
    response.cookie("logged-in", "true")

    console.dir(request.cookies)

    response.send('kroppen, også kalt body\n\n')
})

app.listen(3000, () => {
    console.log('HTTP-server kjører på port 3000')
})