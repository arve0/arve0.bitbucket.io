import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      postnummer: '1234',
      poststed: '',
    }
  }

  handleInput = (event) => {
    const postnummer = event.target.value

    var clientUrl = window.location.href
    var URL = `https://api.bring.com/shippingguide/api/postalCode.json?clientUrl=${clientUrl}&pnr=${postnummer}`

    fetch(URL).then(response => response.json())
      .then((fraBring) => {
        // oppdater tilstand
        this.setState({
          poststed: fraBring.result,
          postnummer: postnummer,
        })
      })
  }

  render() {
    return (
      <div className="App">
        <input defaultValue={this.state.postnummer} onChange={this.handleInput} />
        <br />
        <p>{this.state.poststed}</p>
      </div>
    );
  }
}

export default App;
